export const fetchBrastlewarkData = async () => {
  return await fetch(
    "https://raw.githubusercontent.com/nedap/healthcare-mobile-public/main/data.json"
  )
    .then(res => res.json())

}