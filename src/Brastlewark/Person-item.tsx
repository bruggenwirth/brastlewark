import {Person, UseBrastlewarkContext} from "./Provider";
import styled from "@emotion/styled";

const PersonComponent = styled.div`
  cursor: pointer;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 5px;
  
  &:hover {
    background-color: #f5f5f5;
  }
`

function PersonItem({person}: {person: Person}) {
  const { setCurrentPerson } = UseBrastlewarkContext();
  const handleClick = () => {
    setCurrentPerson(person);
  }
  return (
    <PersonComponent onClick={handleClick}>{person.name}</PersonComponent>
  )
}

export default PersonItem;