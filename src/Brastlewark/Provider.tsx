import React, {useEffect} from "react";
import {fetchBrastlewarkData} from "../lib/fetch";

export interface Person {
  id: number;
  name: string;
  thumbnail: string;
  age: number;
  weight: number;
  height: number;
  hair_color: string;
  professions: string[];
  friends: string[];
}

interface BrastlewarkContextType {
  persons: Person [] | null;
  currentPerson: Person | null;
  setCurrentPerson: (person: Person) => void;
}

const BraslewarkContext = React.createContext<BrastlewarkContextType>({persons: null, currentPerson: null, setCurrentPerson: () => {}});
export const UseBrastlewarkContext = () => React.useContext(BraslewarkContext);

function BrastlewarkProvider({ children }: any) {
  const localstorage = localStorage.getItem('brastlewark') as string;
  const localData = JSON.parse(localstorage);

  const [persons, setPersons] = React.useState<any>(localData || null);
  const [currentPerson, setCurrentPerson] = React.useState<any>(null);

  useEffect(() => {
    async function fetchData() {
      const {Brastlewark} = await fetchBrastlewarkData();
      setPersons(Brastlewark);
      localStorage.setItem('brastlewark', JSON.stringify(Brastlewark));
    }
    fetchData();
  },[])

  return (
    <BraslewarkContext.Provider value={{persons, currentPerson, setCurrentPerson}}>{children}</BraslewarkContext.Provider>
  );
}
export default BrastlewarkProvider;
