import React from "react";
import {Person} from "./Provider";
import styled from "@emotion/styled";

const FriendComponent = styled.div`
  display: inline-block;
  margin-right: 5px;
`;

const Friend = ({friend}: {friend: string}) => {
  return (
    <FriendComponent>
      <strong>{friend}</strong>
    </FriendComponent>
  )
}

function Friends({person}: {person: Person}){

  return (
    <React.Fragment>
      friends: {person.friends.map(friend => <Friend key={friend} friend={friend} />)}
    </React.Fragment>
  )
}
export default Friends;