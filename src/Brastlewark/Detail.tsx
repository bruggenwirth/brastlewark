import styled from "@emotion/styled";
import {UseBrastlewarkContext} from "./Provider";
import Professions from "./Professions";
import Friends from "./Friends";

const DetailComponent = styled.div`
  position: sticky;
  top: 0;
  padding: 50px;
  height: 100vh;
`;

const DetailImage = styled.img`
  margin-top: 30px;
  max-width: 800px;
  max-height: 800px;
`;

const DetailItem = styled.div`
  margin-top: 20px;
`;

function Detail(){
  const { currentPerson } = UseBrastlewarkContext();

  if (!currentPerson) return (
    <DetailComponent>
      <h1>Select a person</h1>
    </DetailComponent>
  );
  return (
    <DetailComponent>
      <h1>{currentPerson.name}</h1>
      <DetailItem>age: <strong>{currentPerson.age}</strong> | weight: <strong>{currentPerson.weight}</strong> | height: <strong>{currentPerson.height}</strong> | hair color: <strong>{currentPerson.hair_color}</strong></DetailItem>
      <DetailImage src={currentPerson.thumbnail} alt={currentPerson.name}/>
      {currentPerson.professions.length > 0 && <DetailItem><Professions person={currentPerson} /></DetailItem>}
      {currentPerson.friends.length > 0 && <DetailItem><Friends person={currentPerson} /></DetailItem>}
    </DetailComponent>
  )
}
export default Detail;