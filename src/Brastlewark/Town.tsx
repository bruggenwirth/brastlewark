import Select from 'react-select'
import {Person, UseBrastlewarkContext} from "./Provider";
import styled from "@emotion/styled";
import PersonItem from "./Person-item";
import {useState} from "react";

const TownComponent = styled.div`
  padding: 50px;
`;

const List = styled.div``;

function onlyUnique(value: any, index: any, self: string | any[]) {
  return self.indexOf(value) === index;
}

const reduceFilterOptions = (persons: Person[] | null) => {
  const professions = persons?.reduce((acc: string[], person: Person) => {
    return [...acc, ...person.professions]
  }, []).filter(onlyUnique).map((profession: string) => {
    return {value: profession, label: profession}
  }) || [];

  const friends = persons?.reduce((acc: string[], person: Person) => {
    return [...acc, ...person.friends]
  }, []).filter(onlyUnique).map((friend: string) => {
    return {value: friend, label: friend}
  }) || [];

  return {professions, friends}
}

function Town(){
  const { persons } = UseBrastlewarkContext();
  const [townPersons, setTownPersons] = useState<Person[] | null>(persons);
  const filterOptions = reduceFilterOptions(persons);

  const handleFilter = (selected: any, name:string) => {
    console.log(name)
    console.log(selected);
    // Hier moet nog filtering komen.

  };

  return (
    <TownComponent>
      <div>
        <Select options={filterOptions.professions} onChange={(e) => handleFilter(e, 'profession')} />
        <Select options={filterOptions.friends} onChange={(e) => handleFilter(e, 'friend')} />
      </div>
      <List>
        {townPersons?.map((person: Person) => <PersonItem person={person} key={person.id}/>)}
      </List>
    </TownComponent>
  )
}
export default Town;