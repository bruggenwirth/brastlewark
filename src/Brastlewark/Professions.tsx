import React from "react";
import {Person} from "./Provider";
import styled from "@emotion/styled";

const ProfessionsComponent = styled.div`
  display: inline-block;
  margin-right: 5px;
`;

const Profession = ({profession}: {profession: string}) => {
  return (
        <ProfessionsComponent>
            <strong>{profession}</strong>
        </ProfessionsComponent>
    )
}

function Professions({person}: {person: Person}){

  return (
    <React.Fragment>
      Professions: {person.professions.map(profession => <Profession key={profession} profession={profession} />)}
    </React.Fragment>
  )
}
export default Professions;