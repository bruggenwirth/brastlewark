import React from 'react';
import BrastlewarkProvider from "./Brastlewark/Provider";
import Town from "./Brastlewark/Town";
import Detail from "./Brastlewark/Detail";
import styled from "@emotion/styled";

const Page = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: 1fr 3fr;
`;

function App() {
  return (
    <BrastlewarkProvider>
      <Page>
        <Town/>
        <Detail />
      </Page>
    </BrastlewarkProvider>
  );
}

export default App;
